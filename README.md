##Installation##

1. npm install -g
2. Create a `.bbprrc` file in your HOME directory and/or your repo's root directory. These will be merged but your repo directory's configuration will take precedence. You can also set your username and password as Enviroment Variables `bbpr_username` and `bbpr_password`.
3. `bbpr --help` for options


##Configuration Options##

These can be stored in a JSON file called .bbpprc either in your $HOME directory and/or repository.   
The JSON objects will be merged, with the $HOME directory taking precedence.  
Another option is to have environment variables called `bbpr_<option>` where <option> is the name of the key (username, password, etc).  

**username** - Your bitbucket username

**password** - Your bitbucket password

**defaultReviewers** - An array or comma seperated string of reviewers

**reviewerMappings** - A map of name => bitbucket username. This is for convenience so you don't have to remember the bitbucket names of others.

**repoPath** - The slug for the repo. For example, `bibliocommons/core-bibliocommons`